Installation process:

    1) Execute install_init.sh with root priviledges
    
    2) Reboot the system
    
    3) Execute install_system_config.sh with root priviledges
    
    4) Execute install_user_config.sh with no root priviledges

    5) Reboot the system 
    
    
After runnning all the install scripts you need to:

    *Open Thunar and use the custom command "gvim --remote-tab" to open
text/config files in new tabs in vim

    *If you want to add permanent network storage, enter the storage address
into thunar, once added right click on the storage and click "Create Shortcut"

    *If you want to change the wallpaper you have to execute change_wallpaper.sh
    with the wallpaper you want as an argument
