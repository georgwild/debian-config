#!/bin/bash
#This script can be used to change the wallpaper in openbox using feh

#Check if directory for wallpapers is existent, othewise create it
if [ ! -d "$HOME/Pictures/wallpapers" ]; then
  mkdir $HOME/Pictures $HOME/Pictures/wallpapers
  echo "Your current wallpaper is stored in $HOME/Pictures/wallpapers"
fi

#Delete current wallpaper
rm -f $HOME/Pictures/wallpapers/current-wp.png $HOME/Pictures/wallpapers/current-wp.jpg

#Check if wallpaper is a PNG or JPEG and act accordingly
if [ ${1: -4} == ".png" ]; then
  cp -f $1 $HOME/Pictures/wallpapers/current-wp.png
  feh --bg-scale $HOME/Pictures/wallpapers/current-wp.png
elif [ ${1: -4} == ".jpg" ]; then
  cp -f $1 $HOME/Pictures/wallpapers/current-wp.jpg
  feh --bg-scale $HOME/Pictures/wallpapers/current-wp.jpg
fi

echo "Wallpaper changed"