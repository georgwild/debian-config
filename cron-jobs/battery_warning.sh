#!/bin/bash

STATE=`/usr/bin/acpi -b`

if [[ `/bin/echo $STATE | /bin/grep Discharging` && `/bin/echo $STATE |
/usr/bin/cut -f 5 -d " "` < 00:15:00 ]] ;
then

DISPLAY=0.0 XAUTHORITY=/home/georg/.Xauthority DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/1000/bus /usr/bin/notify-send -u critical -t 200000 -i dialog-warning -c device "Battery Low" "Battery level at 15%. Plug in your charger."

fi
