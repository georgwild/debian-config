#!/bin/bash

#This script installs all necessary packages from the debian repositories

#Install sudo
apt-get -my install sudo

#Install Laptop power management tools
apt-get -my install laptop-mode-tools acpid linux-cpupower

#Install x-window-system
apt-get -my install xorg

#Install display manager and lock-screen
apt-get -my install lightdm light-locker

#Install window manager (Openbox), Openbox configuration tools (obconf, obmenu),
#Taskbar (tint2), Composite Manager (compton), Application menu (menu),
#background manager (feh), network control (wicd), volume control (alsa-utils, alsamixergui,
# volumeicon-alsa)
apt-get -my install openbox obconf obmenu tint2 compton compton-conf gtk2-engines-murrine menu feh wicd pulseaudio pavucontrol volumeicon-alsa lxappearance blueman xautolock dmenu policykit-1 lxpolkit arandr xfce4-notifyd

#Packeges for printing and scanning
apt-get -my install task-print-server system-config-printer libsane simple-scan

#Install user programms
apt-get -my install sakura xfce4-taskmanager thunar thunar-archive-plugin thunar-volman gvfs gvfs-* xarchiver medit firefox-esr thunderbird vim vim-gtk3 vim-syntastic atril
