#!/bin/bash
#This script has to be executed as the super user

#enable Ctrl+Alt+Backspace shortcut to kill the current X-Session
#also fix screen tearing for intel drivers
#to do that insert some lines into /etc/X11/xorg.conf

#Commented out because they break system after reboot
: '

if [ ! -f /etc/X11/xorg.conf ]; then
  touch /etc/X11/xorg.conf
  cat > /etc/X11/xorg.conf << EOL

#Fix Screen tearing for Intel graphics
Section "Device"
   Identifier  "Intel Graphics"
   Driver      "intel"
   Option      "AccelMethod" "sna"
   Option      "TearFree" "true"
EndSection

#This section enables the shortcut Ctrl+Alt+Backspace to kill the current X-Session
Section "ServerFlags"
    Option "DontZap" "false"
EndSection

Section "InputClass"
    Identifier      "Keyboard Defaults"
    MatchIsKeyboard "yes"
    Option          "XkbOptions" "terminate:ctrl_alt_bksp"
EndSection

EOL

elif ( ! grep -Fq "terminate:ctrl_alt_bksp" /etc/X11/xorg.conf ); then
    cat >> /etc/X11/xorg.conf << EOL

#Fix Screen tearing for Intel graphics
Section "Device"
   Identifier  "Intel Graphics"
   Driver      "intel"
   Option      "AccelMethod" "sna"
   Option      "TearFree" "true"
EndSection

#This section enables the shortcut Ctrl+Alt+Backspace to kill the current X-Session
Section "ServerFlags"
    Option "DontZap" "false"
EndSection

Section "InputClass"
    Identifier      "Keyboard Defaults"
    MatchIsKeyboard "yes"
    Option          "XkbOptions" "terminate:ctrl_alt_bksp"
EndSection

EOL
fi

'

#Make the Power commands in the openbox menu work with sudo
#Check if the provided argument is not zero and in fact a valid username
if [ $# -eq 0 ] || [ -z "$1" ] ; then
  echo "Your username is nedded as an argument!"
  exit 1
elif ( ! grep -Fq "$1" /etc/passwd ); then
  echo "This user does not exist"
  exit 1
fi

#Add the specified user to the group "users"
usermod $1 -aG users

#Add the needed lines to the sudoers file
if ( ! grep -Fq "#Privileges for shutdown commands" /etc/sudoers ); then
  echo "" | sudo EDITOR='tee -a' visudo
  echo "#Privileges for shutdown commands" | sudo EDITOR='tee -a' visudo
fi

if ( ! grep -Fq "%users ALL=(root) NOPASSWD: /bin/systemctl halt, /bin/systemctl poweroff, /bin/systemctl reboot, /bin/systemctl suspend, /bin/systemctl hibernate" /etc/sudoers ); then
  echo "%users ALL=(root) NOPASSWD: /bin/systemctl halt, /bin/systemctl poweroff, /bin/systemctl reboot, /bin/systemctl suspend, /bin/systemctl hibernate" | sudo EDITOR='tee -a' visudo
fi

#Make light-locker start after suspend, hibernate or hybrid-sleep
cp ./systemd/resume-system@.service ./systemd/alsa-audio@.service ./systemd/alsa-audio.sh /etc/systemd/system/
systemctl enabel resume-system@$1
systemctl enabel alsa-audio@$1

#set openbox as the standard window and session manager
update-alternatives --config x-window-manager
update-alternatives --config x-session-manager

#set standard terminal emulator
update-alternatives --config x-terminal-emulator

#Copy GTK-Themes, Icon-Themes and Cursor-Themes to their destinations
cp -rfv ./gtk-themes/Wild-Theme /usr/share/themes/
cp -rfv ./gtk-themes/Numix /usr/share/themes/
cp -rfv ./icon-themes/Numix /usr/share/icons/
cp -rfv ./cursor-themes/capitaine-cursors /usr/share/icons/

#set lightdm as the standard display manager and use custom config files
dpkg-reconfigure lightdm
cp -rfv ./lightdm/ /etc/

#set openbox systemwide config
cp -rfv ./openbox/ /etc/xdg/
rm /etc/xdg/openbox/autostart
touch /etc/xdg/openbox/autostart

#set root user configurations
cp -rfv ./bash/root/.bashrc $HOME/
mkdir -v $HOME/.config
cp -rfv ./sakura $HOME/.config/
rm -rf /usr/bin/xterm
ln -s sakura /usr/bin/xterm
cp -rfv ./openbox $HOME/.config/
cp -rfv ./tint2 $HOME/.config/
cp -rfv ./gtk-settings/gtk-2.0 $HOME/.config/
cp -rfv ./gtk-settings/gtk-3.0 $HOME/.config/
cp -rfv ./gtk-settings/.gtkrc-2.0 $HOME/
cp -rfv ./xfce4 $HOME/.config/
cp -rfv ./vim/.vimrc $HOME/
cp -rfv ./vim/.vim $HOME/

#set pulseaudio config (so that it doesnt autostart all the time)
cp -rfv ./pulseaudio/client.conf /etc/pulse/

#copy battery warning script to /usr/local/bin
cp -rfv ./cron-jobs/battery_warning.sh /usr/local/bin/battery_warning.sh
