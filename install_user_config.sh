#!/bin/bash

#This script has to be executed as the user and not as su

#Move the openbox configuration files to their home directory destination
cp -rfv ./openbox $HOME/.config/

#Move the tint2 configuration files to their home directory destination
cp -rfv ./tint2 $HOME/.config/

#Move the gtk configuration files to their home directory destination
cp -rfv ./gtk-settings/gtk-2.0 $HOME/.config/
cp -rfv ./gtk-settings/gtk-3.0 $HOME/.config/
cp -rfv ./gtk-settings/.gtkrc-2.0 $HOME/

#Move the .bashrc configuration file to its home directory destination
cp -rfv ./bash/default-user/.bashrc $HOME/

#Move the volumeicon config file to its place
cp -rfv ./volumeicon $HOME/.config/

#Move the sakura configuration file to its home directory destination
cp -rfv ./sakura/sakura.conf $HOME/.config/sakura/

#Move the xfce4 config files to their home directory destination
cp -rfv ./xfce4 $HOME/.config/

#Move the vim config files to their home directory destination
cp -rfv ./vim/.vimrc $HOME/
cp -rfv ./vim/.vim $HOME/

#Move initial wallpaper to its home directory destination
mkdir $HOME/Pictures/wallpapers
cp -rfv ./wallpaper/current-wp.jpg $HOME/Pictures/wallpapers/current-wp.jpg

#Create crontab to run battery warning script every 5 minutes (The script is copied by root)
crontab -l > tempcron
echo "SHELL=/bin/bash" >> tempcron
echo "*/5 * * * * /usr/local/bin/battery_warning.sh" >> tempcron
crontab tempcron
rm -fv tempcron

#Move the compton configuration files to their home directory destination
#cp -v ./compton/compton.conf $HOME/.config/
