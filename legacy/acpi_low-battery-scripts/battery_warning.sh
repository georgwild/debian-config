#!/bin/sh

read -r state</sys/class/power_supply/BAT1/capacity

export DISPLAY=:0.0

if [ $state -lt "15" ]; 
then

notify-send -u critical -t 20000 -i dialog-warning -c device "Battery Low" \
"Battery level at 15%. Plug in your charger."

fi
