### Wild theme for SLiM
### This theme is based on the slim-minimal and the debian-lines theme
## background
# style of background: 'stretch', 'tile', 'center', 'color'
background_style        tile
background_color        #222222

# messages such as shutdown, reboot, username for slimlock
msg_x                   20%
msg_y                   48%
msg_color               #ffffff
msg_font                Verdana:size=15:dpi=96
#msg_shadow_xoffset     1
#msg_shadow_yoffset     1
#msg_shadow_color       #ff00ff

# session messages eg: pressing F1:
session_x		20%
session_y		65%
session_font		Verdana:size=15:dpi=96
session_color		#ffffff
#session_shadow_xoffset 1
#session_shadow_yoffset 1
#session_shadow_color	#000000

# Welcome message position. (relative to the panel)
# use -1 for both values or comment the options to disable
# the welcome message
welcome_x		-1 #15%
welcome_y		-1 #30%
welcome_font		Verdana:size=16:bold:slant=italic:dpi=96
welcome_color		#ffffff
#welcome_shadow_xoffset 1
#welcome_shadow_yoffset 2
#welcome_shadow_color	#444444

## panel
# Horizonatal and vertical position for the panel.
input_panel_x		20%
input_panel_y		50%

# input controls horizontal and vertical positions.
# IMPORTANT! set input_pass_x and input_pass_y to -1
# to use a single input box for username/password (GDM Style).
# Note that this fields only accept absolute values.
input_name_x            90
input_name_y            135
input_pass_x            -1
input_pass_y            -1
# Input controls font and color
input_font              Verdana:size=12:bold:dpi=96
input_fgcolor           #ffffff
input_color             #ffffff
#input_shadow_xoffset   1
#input_shadow_yoffset   1
#input_shadow_color     #ff00ff

# 'Enter username' and 'Enter password' position(relative to the panel)
# use -1 for both values to disable the message
# note that in case of single inputbox the password values are ignored.
username_x              140
username_y              65
password_x              145
password_y              65
username_font           Verdana:size=15:dpi=96
username_color          #eeeeee
#username_shadow_xoffset -1
#username_shadow_yoffset -1
#username_shadow_color	#ff00ff

# The message to be displayed. Leave blank if no message
# is needed (ie, when already present in the panel image)
#
username_msg            Username
password_msg            Password

### slimlock taken from: 
### https://github.com/data-modul/slim/blob/master/slimlock.conf
# number of seconds of inactivity before the screen blanks. Default: 60
dpms_standby_timeout	60

# same as above, but the screen turns off. Default: 600
dpms_off_timeout	600

# number of seconds after entering an incorrect password before slimlock
# will accept another attempt. Default: 2
wrong_passwd_timeout	1

# message to display after a failed authentication attempt.
# Default: "Authentication failed"
passwd_feedback_msg	Authentication failed

# message to display after a failed authentication attempt if the
# CapsLock is on. Default: "Authentication failed (CapsLock is on)"
passwd_feedback_capslock Authentication failed (CapsLock is on)

# whether or not to display the username on themes with only a single
# input box. 1 to show, 0 to disable. Default: 1
show_username		1

# whether or not to display SLiM's welcome message.
# 1 to show, 0 to disable. Default: 0
show_welcome_msg	0

# whether or not to allow virtual terminals switching.
# 1 to disallow, 0 to allow. Default: 1
tty_lock		1

# whether to ring the bell on authentication failure.
# 1 to enable, 0 to disable. Default: 1
bell			1

# character to display when masking password.
# Only the first character is used. Default: *
passwd_char		*