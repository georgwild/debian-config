set softtabstop=4 "Controls how many columns are used for tab in insert mode 
set shiftwidth=4 "Number of spaces to use for autoindentation 

set wrap "Display too long lines on multiple lines
set textwidth=0 "Ensures that lines have no lenght limit 
set wrapmargin=0 "Ensures that lines have no lenght limit 

set laststatus=2 "Set statusline to be always visible

set lines=45 columns=100

syntax on "Enable syntax highlighting
set number "Enable line numbers

"GVIM specific settings to hide tool- and menubar
set guioptions-=m
set guioptions-=T

colorscheme Tomorrow-Night-Bright

let g:currentmode={
      \ 'n'  : 'Normal',
      \ 'no' : 'N·Operator Pending',
      \ 'v'  : 'Visual',
      \ 'V'  : 'V·Line',
      \ 'x22' : 'V·Block',
      \ 's'  : 'Select',
      \ 'S'  : 'S·Line',
      \ 'x19' : 'S·Block',
      \ 'i'  : 'Insert',
      \ 'R'  : 'Replace',
      \ 'Rv' : 'V·Replace',
      \ 'c'  : 'Command',
      \ 'cv' : 'Vim Ex',
      \ 'ce' : 'Ex',
      \ 'r'  : 'Prompt',
      \ 'rm' : 'More',
      \ 'r?' : 'Confirm',
      \ '!'  : 'Shell',
      \ 't'  : 'Terminal'
      \}

" Find out current buffer's size and output it.
function! FileSize()
  let bytes = getfsize(expand('%:p'))
  if (bytes >= 1024)
    let kbytes = bytes / 1024
  endif
  if (exists('kbytes') && kbytes >= 1000)
    let mbytes = kbytes / 1000
  endif

  if bytes <= 0
    return '0'
  endif

  if (exists('mbytes'))
    return mbytes . 'MB '
  elseif (exists('kbytes'))
    return kbytes . 'KB '
  else
    return bytes . 'B '
  endif
endfunction

"Highlight groups for dynamic status bar colors
hi StatusNormal1 cterm=bold ctermfg=117 ctermbg=24 guifg=#87d7ff guibg=#005f87
hi StatusNormal2 cterm=bold ctermfg=235 ctermbg=111 guifg=#262626 guibg=#87afff
hi StatusNormal3 cterm=bold ctermfg=235 ctermbg=152 guifg=#262626 guibg=#afd7d7
hi StatusInsert1 cterm=bold ctermfg=191 ctermbg=58 guifg=#d7ff5f guibg=#5f5f00
hi StatusInsert2 cterm=bold ctermfg=235 ctermbg=142 guifg=#262626 guibg=#afaf00
hi StatusInsert3 cterm=bold ctermfg=235 ctermbg=192 guifg=#262626 guibg=#d7ff87
hi StatusVisual1 cterm=bold ctermfg=137 ctermbg=94 guifg=#af875f guibg=#875f00
hi StatusVisual2 cterm=bold ctermfg=235 ctermbg=221 guifg=#262626 guibg=#ffd75f
hi StatusVisual3 cterm=bold ctermfg=235 ctermbg=180 guifg=#262626 guibg=#d7af87
hi StatusReplace1 cterm=bold ctermfg=203 ctermbg=88 guifg=#ff5f5f guibg=#870000
hi StatusReplace2 cterm=bold ctermfg=235 ctermbg=160 guifg=#262626 guibg=#d70000
hi StatusReplace3 cterm=bold ctermfg=235 ctermbg=203 guifg=#262626 guibg=#ff5f5f

function! ChangeStatuslineColor()
  if (mode() =~# '\v(n|no)')
    exe 'hi link User1 StatusNormal1'
    exe 'hi link User2 StatusNormal2'
    exe 'hi link User3 StatusNormal3'
  elseif (mode() =~# '\v(v|V)' || g:currentmode[mode()] ==# 'V·Block' || get(g:currentmode, mode(), '') ==# 't')
    exe 'hi link User1 StatusVisual1'
    exe 'hi link User2 StatusVisual2'
    exe 'hi link User3 StatusVisual3'
  elseif (mode() ==# 'i')
    exe 'hi link User1 StatusInsert1'
    exe 'hi link User2 StatusInsert2'
    exe 'hi link User3 StatusInsert3'
  else
    exe 'hi link User1 StatusReplace1'
    exe 'hi link User2 StatusReplace2'
    exe 'hi link User3 StatusReplace3'
endif
  return ''
endfunction

"change components of statusline"change components of statusline
set statusline=
"set statusline+=%#IncSearch#
set statusline+=%{ChangeStatuslineColor()}
set statusline+=%#User1#
set statusline+=\ 
set statusline+=%{toupper(g:currentmode[mode()])}\  " Current mode
set statusline+=%#User2#
set statusline+=\ 
set statusline+=%.30F
set statusline+=\ 
set statusline+=%#User3#
set statusline+=%=
set statusline+=%#User2#
set statusline+=\ 
set statusline+=%{(&fenc!=''?&fenc:&enc)}\[%{&ff}]
set statusline+=\ 
set statusline+=%y 
set statusline+=\ 
set statusline+=%#User1#
set statusline+=\ 
set statusline+=%{FileSize()}\  "File size
set statusline+=%l
set statusline+=/
set statusline+=%L\ 
